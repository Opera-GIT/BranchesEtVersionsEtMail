######################################
# PARAMETRES
######################################
Write-Host $([System.DateTime]::Now.ToString("dd/MM/yyyy HH:mm")) " ### Lancement du Script"
$FromRev = "1.12.08-F9-Livraison_BI"

$FileName = "BranchesEtVersions.svg"
$LightFileName = "LightBranchesEtVersions."+$([System.DateTime]::Now.ToString("yyyy-MM-dd-HH-mm"))+".png"

$RepoGIT = "D:\OPERA\OPERA-Tree"
$FilePath = "D:\OPERA\" + $FileName
$LightFilePath = "D:\OPERA\" + $LightFileName

$Subject = "Cartographie BranchesTags du " + [System.DateTime]::Now.ToString("dd/MM/yyyy HH:mm")
#$Attachment = $LightFilePath, $FilePath
$Attachment = $LightFilePath
$Body = "<html><img src='$LightFileName'></html>";

$From = "opera.git@gmail.com"
$To = "gerard.walace@gmail.com"
$SMTPServer = "smtp.gmail.com"
$SMTPPort = "25"
$username = "opera.git@gmail.com"
$password = convertto-securestring -String "notverysecretpassword" -AsPlainText -Force
$Credential = new-object -typename System.Management.Automation.PSCredential -argumentlist $username, $password

######################################
# GIT Fetch & REVISION GRAPH
######################################
Write-Host $([System.DateTime]::Now.ToString("dd/MM/yyyy HH:mm")) " ### Fetch du repository"
git.exe -C $RepoGIT fetch --prune --tags "origin"

# On supprime les fichiers pour pouvoir tester leur existence
Write-Host $([System.DateTime]::Now.ToString("dd/MM/yyyy HH:mm")) " ### Suppression des anciennes images"
if (Test-Path $FilePath) {del $FilePath}
if (Test-Path $LightFilePath) {del $LightFilePath}

Write-Host $([System.DateTime]::Now.ToString("dd/MM/yyyy HH:mm")) " ### Lancement du traitement Light"
TortoiseGitProc.exe /command:revisiongraph /path:$RepoGIT /startrev:$FromRev /output:$LightFilePath
# On attend tant que le fichier n'est pas créé (et rempli)
while (-Not (Test-Path $LightFilePath)) {
	Start-Sleep -s 30
	Write-Host $([System.DateTime]::Now.ToString("dd/MM/yyyy HH:mm")) " ### Traitement Light en cours..."
}
do {
	Write-Host $([System.DateTime]::Now.ToString("dd/MM/yyyy HH:mm")) " ### Fin du traitement. Copie de l'image sur le disque..."
	Start-Sleep -s 30
} while (-Not ((Get-Item $LightFilePath).length -gt 0kb))

#Write-Host $([System.DateTime]::Now.ToString("dd/MM/yyyy HH:mm")) " ### Lancement du traitement LOURD..."
#TortoiseGitProc.exe /command:revisiongraph /path:$RepoGIT /withcommitcount /startrev:$FromRev /output:$FilePath
## On attend tant que le fichier n'est pas créé (et rempli)
#while (-Not (Test-Path $FilePath)) {
#	Start-Sleep -s 30
#	Write-Host $([System.DateTime]::Now.ToString("dd/MM/yyyy HH:mm")) " ### Traitement LOURD en cours..."
#}
#do {
#	Write-Host $([System.DateTime]::Now.ToString("dd/MM/yyyy HH:mm")) " ### Fin du traitement. Copie de l'image sur le disque..."
#	Start-Sleep -s 30
#} while (-Not ((Get-Item $FilePath).length -gt 0kb))


######################################
# ENVOI MAIL
######################################
Write-Host $([System.DateTime]::Now.ToString("dd/MM/yyyy HH:mm")) " ### Envoi des images par email..."

Send-MailMessage -From $From -to $To -Subject $Subject -Body $Body -BodyAsHtml -SmtpServer $SMTPServer -port $SMTPPort -UseSsl -Credential $Credential -Attachments $Attachment

Write-Host $([System.DateTime]::Now.ToString("dd/MM/yyyy HH:mm")) " ### Fin du traitement. On quitte !!!"
pause
